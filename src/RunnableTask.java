import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.RecursiveTask;
import java.util.Random;
import java.util.concurrent.Callable;

public class RunnableTask implements Runnable {
	
	private String [][] matriz;
	private String [][] resultado;
	private int  [][] filtro;
	private int fin;
	private int inicio;
	Filter f;
	
	public RunnableTask(String [][] matriz,int [][] filtro,String [][] resultado,int iteracion,int fin) throws FileNotFoundException{
		this.matriz = matriz;
		this.filtro = filtro;
		this.resultado = resultado;
		this.inicio = iteracion;
		this.fin = fin;
		this.f = new Filter(); 
		
	}

	@Override
	public void run() {
		for (int i=0;i<matriz[this.inicio].length;i++) {
			//Thread.sleep(500+new Random().nextInt(1000));
			 efecto(this.matriz,this.filtro,this.resultado,inicio,i);
		}
	/*	try {
			this.f.escribirFichero(this.resultado, this.resultado.length,this.resultado[0].length,255,"salida");
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}


	
	public void efecto(String[][] matriz,int[][] filtro,String [][] resultado,int i, int j ){
		int nuevoValor;
		int [][] adyacentes = new int [3][3];
		//String [][] columnaResult = new String[matriz.length][1];
		//for(int i=0;i<matriz.length;i++){
		//	 for(int j=inicio;j==inicio;j++){
				String diagSupIzq = (i-1>=0 && j-1>=0)? matriz[i-1][j-1]:"0";
				String izq = (j-1>=0)? matriz[i][j-1]:"0";
				String diagInfIzq = (i+1<matriz.length && j-1>=0)? matriz[i+1][j-1]:"0";
				String sup = (i-1>=0 )? matriz[i-1][j]:"0";
				String inf = (i+1<matriz.length-1)? matriz[i+1][fin]:"0";
				String diagSupDer = (i-1>=0 && j+1<matriz[0].length)? matriz[i-1][j+1]:"0";
				String der = (j+1<matriz[0].length)? matriz[i][j+1]:"0";
				String diagInfDer = (i+1<matriz.length && j+1<matriz[0].length)? matriz[i+1][j+1]:"0";
				
				String centro = matriz[i][j];
				
				adyacentes[0][0] = Integer.parseInt(diagSupIzq);
				adyacentes[0][1] = Integer.parseInt(sup);
				adyacentes[0][2] = Integer.parseInt(diagSupDer);
				adyacentes[1][0] = Integer.parseInt(izq);
				adyacentes[1][1] = Integer.parseInt(centro);
				adyacentes[1][2] = Integer.parseInt(der);
				adyacentes[2][0] = Integer.parseInt(diagInfIzq);
				adyacentes[2][1] = Integer.parseInt(inf);
				adyacentes[2][2] = Integer.parseInt(diagInfDer);
				
				nuevoValor = calcularValor(adyacentes,filtro);
				this.resultado[i][j] = Integer.toString(nuevoValor);
				//columnaResult[i][0] = Integer.toString(nuevoValor);
				if(i==this.matriz.length-1 && j == this.matriz[0].length-1){
				try {
					this.f.escribirFichero(this.resultado, this.resultado.length,this.resultado[0].length,255,"salida");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
			// }
		//}
		
	}
	
	public String [][] combinarColumnas(String[][] izquierda, String [][] derecha,int mitad){
		int ancho = derecha[0].length+izquierda[0].length;
		String [][] resultado = new String [izquierda.length][ancho];
		 for(int i=0;i<izquierda.length;i++){
			 for(int j=0;j<ancho;j++){
				 if(j<izquierda[0].length){
					 resultado [i][j] = izquierda[i][j];
				 }else{
					 resultado [i][j] = derecha[i][j-izquierda[0].length];
				 }
				 
			 }
		 }
		 return resultado;
	}
	
	//crear metodo para rellenar los adyacentes

	
	private int calcularValor(int [][] valores, int [][] filtro){
		int resultado = 0;
		int positivo = 0;
		int negativo = 0;
		
		for(int i=0;i<valores.length;i++){
			for(int j=0;j<valores[i].length;j++){
				
				resultado += valores[i][j]*filtro[i][j];
				if(filtro[i][j]>0){
					positivo+=filtro[i][j];
				}
				else{
					if(filtro[i][j]<0){
						negativo+=filtro[i][j];
					}
				}
			}
		}
		int escalar = Math.max(positivo, Math.abs(negativo));
		resultado = Math.abs(resultado);
		return resultado/escalar;
	}

	
	
	
}
