import java.util.concurrent.RecursiveTask;


public class FJFilter extends RecursiveTask<String[][]> {
	
	private String [][] matriz;
	private int  [][] filtro;
	private int fin;
	private int inicio;
	
	public FJFilter(String [][] matriz,int [][] filtro,int inicio,int fin){
		this.matriz = matriz;
		this.filtro = filtro;
		this.inicio = inicio;
		this.fin = fin;
		
	}

	@Override
	protected String [][] compute() {
		
		if(this.inicio==this.fin){
			return efecto(this.matriz,this.filtro,this.inicio,this.fin);
		}
		else{
			int mitad = (this.inicio+this.fin)/2;
			//System.out.println(mitad);
			FJFilter izq = new FJFilter(this.matriz,this.filtro,inicio,mitad);
			FJFilter der = new FJFilter(this.matriz,this.filtro,mitad+1,fin);
			izq.fork();
			String [][] derResult = der.compute();
			//der.fork();
			String [][] izqResult = izq.join();
			//String [][] derResult = der.join();
			
			return combinarColumnas(izqResult,derResult,mitad);
		}
	}
	
	
	public String[][] efecto(String[][] matriz,int[][] filtro,int inicio, int fin ){
		int nuevoValor;
		int [][] adyacentes = new int [3][3];
		String [][] columnaResult = new String[matriz.length][1];
		for(int i=0;i<matriz.length;i++){
			 for(int j=inicio;j==inicio;j++){
				String diagSupIzq = (i-1>=0 && j-1>=0)? matriz[i-1][j-1]:"0";
				String izq = (j-1>=0)? matriz[i][j-1]:"0";
				String diagInfIzq = (i+1<matriz.length && j-1>=0)? matriz[i+1][j-1]:"0";
				String sup = (i-1>=0 )? matriz[i-1][j]:"0";
				String inf = (i+1<matriz.length-1)? matriz[i+1][fin]:"0";
				String diagSupDer = (i-1>=0 && j+1<matriz[0].length)? matriz[i-1][j+1]:"0";
				String der = (j+1<matriz[0].length)? matriz[i][j+1]:"0";
				String diagInfDer = (i+1<matriz.length && j+1<matriz[0].length)? matriz[i+1][j+1]:"0";
				
				String centro = matriz[i][j];
				
				adyacentes[0][0] = Integer.parseInt(diagSupIzq);
				adyacentes[0][1] = Integer.parseInt(sup);
				adyacentes[0][2] = Integer.parseInt(diagSupDer);
				adyacentes[1][0] = Integer.parseInt(izq);
				adyacentes[1][1] = Integer.parseInt(centro);
				adyacentes[1][2] = Integer.parseInt(der);
				adyacentes[2][0] = Integer.parseInt(diagInfIzq);
				adyacentes[2][1] = Integer.parseInt(inf);
				adyacentes[2][2] = Integer.parseInt(diagInfDer);
				
				nuevoValor = calcularValor(adyacentes,filtro);
				columnaResult[i][0] = Integer.toString(nuevoValor);
			 }
		}
		
		return columnaResult;
	}
	
	public String [][] combinarColumnas(String[][] izquierda, String [][] derecha,int mitad){
		int ancho = derecha[0].length+izquierda[0].length;
		String [][] resultado = new String [izquierda.length][ancho];
		 for(int i=0;i<izquierda.length;i++){
			 for(int j=0;j<ancho;j++){
				 if(j<izquierda[0].length){
					 resultado [i][j] = izquierda[i][j];
				 }else{
					 resultado [i][j] = derecha[i][j-izquierda[0].length];
				 }
				 
			 }
		 }
		 return resultado;
	}
	
	//crear metodo para rellenar los adyacentes

	
	private int calcularValor(int [][] valores, int [][] filtro){
		int resultado = 0;
		int positivo = 0;
		int negativo = 0;
		
		for(int i=0;i<valores.length;i++){
			for(int j=0;j<valores[i].length;j++){
				
				resultado += valores[i][j]*filtro[i][j];
				if(filtro[i][j]>0){
					positivo+=filtro[i][j];
				}
				else{
					if(filtro[i][j]<0){
						negativo+=filtro[i][j];
					}
				}
			}
		}
		int escalar = Math.max(positivo, Math.abs(negativo));
		resultado = Math.abs(resultado);
		return resultado/escalar;
	}
	
	
}
