import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;


public class Filter {
	
	private Reader r;
	private BufferedReader br;
	
	public Filter(){
		
	}
		
	public Filter(String filename) throws FileNotFoundException{
		r = new FileReader(filename);
		br = new BufferedReader(r);
		
	}
	
	public String dimension(String filename) throws IOException{
		String str;
		while((str=br.readLine())!=null){
			if(str.startsWith("#") || str.startsWith("P")){
				continue;
			}
			break;
		}
		str+=" "+br.readLine();
		return str;
		
	}
	
	
	public String [][] volcarMatriz(int alto,int ancho) throws IOException{
		String [] cadena;
		String [][] matrix = new String [alto][ancho];
		String str;
		String pixeles="";
		int index=0;
		List<String> img = new ArrayList<>();
		while((str=br.readLine())!=null){
			pixeles += str;
		}
			cadena = pixeles.split("\\s+");
			for(int i=0;i<cadena.length;i++){
				if(isNumeric(cadena[i])){
					img.add(cadena[i]);
				}
				
			}
		
	
		System.out.println(img.size());
		
		for(int i=0;i<matrix.length;i++){
			for(int j=0;j<matrix[i].length;j++){
				matrix[i][j] = img.get(index);
				index++;
			}
		}
		System.out.println("TEMINADO");
		
		
		return matrix;
	}
	
	
	public void escribirFichero(String [][] matrix,int alto,int ancho, int gris,String nombre) throws IOException{
		Writer w = new FileWriter(nombre+".pgm");
		PrintWriter pw = new PrintWriter(w);
	       
		for(int i=0; i<matrix.length; i++){
			if(i==0){
				pw.println("P2");
				pw.println(ancho+" "+alto);
				pw.println(gris);
			}
	           for(int j=0; j<matrix[i].length; j++){
	                pw.print(matrix[i][j]+ " ");
	           }
	           pw.println();
	       }
		System.out.println("TEMINADO");
		
		if(w!=null){
			w.close();
			pw.close();
		}
	}
	
	private static boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}
}
