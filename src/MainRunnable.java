import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainRunnable{
	private static Writer w;
	private static PrintWriter pw;
	
	
	public static void writeFile(String valor,int i,int ancho, int alto,int gris){
		pw.print(valor+" ");
		
	}

	public static void main(String[] args) throws IOException {
		
		w = new FileWriter("salida.pgm");
		pw = new PrintWriter(w);
		int ancho,alto,gris;
		String [][] matriz;
	       Scanner sc = new Scanner(System.in);
	       System.out.println("-------------------------------- Elige la ruta de la imagen --------------------------------");
	       String ruta = sc.nextLine();
	       Filter f = new Filter(ruta);
	       System.out.println("Procesando matriz, por favor espere...");
	       String dimension = f.dimension(ruta);
	       String [] pixel = dimension.split("\\s+");
	       ancho = Integer.parseInt(pixel[0]);
	       alto = Integer.parseInt(pixel[1]);
	       gris = Integer.parseInt(pixel[2]);
	       
	       pw.println("P2");
			pw.println(ancho+" "+alto);
			pw.println(gris);
	      
	       
	       matriz = new String [alto][ancho];
	       
	       String [][] resultado = new String [alto][ancho];
	       matriz = f.volcarMatriz(alto, ancho);
	       System.out.println("Matriz"+matriz.length+"x"+matriz[0].length);
	       int opcion = 2;
	       while(opcion != 0){
	    	   System.out.println("-------------------------------- Elige el filtro que desesas aplicar --------------------------------");
		       System.out.println("1 - Sobel Horizontal");
		       System.out.println("2 - Sobel Vertical");
		       System.out.println("3 - Enfocar");
		       System.out.println("4 - Deteccion de bordes");
		       System.out.println("5 - Emboss");
		       System.out.println("6 - Desenfocar");
		       opcion = Integer.parseInt(sc.nextLine());
		       System.out.println("*** Procesando, espere por favor... ***");
		       switch (opcion) {
		       
		        case 1:
		        	int [][] sobelH = {{-1,-2,-1},{0,0,0},{1,2,1}};
		        	ExecutorService executor =  Executors.newCachedThreadPool();
		        	
		        	for (int i=0;i<matriz.length;i++) {
		    		    executor.execute(new RunnableTask(matriz,sobelH,resultado,i,ancho-1));
		    		}
		    		executor.shutdown();

		    		break;
		        case 2:
			        int [][] sobelV = {{-1, 0, 1},{-2,0,2},{-1,0,1}};
			        ExecutorService executor2 =  Executors.newCachedThreadPool();
			        for (int i=0;i<matriz.length;i++) {
		    		    executor2.execute(new RunnableTask(matriz,sobelV,resultado,i,ancho-1));
		    		}
		    		executor2.shutdown();
		 	       	System.out.println("*** Imagen procesada correctamente *** ");
		        break;
		        case 3:
		        	int [][] enfocar = {{0, -1,0},{-1,5,-1},{0,-1,0}};
		        	ExecutorService executor3 =  Executors.newCachedThreadPool();
			        for (int i=0;i<matriz.length;i++) {
		    		    executor3.execute(new RunnableTask(matriz,enfocar,resultado,i,ancho-1));
		    		}
		    		executor3.shutdown();
		 	       	System.out.println("*** Imagen procesada correctamente *** ");
			        break;
		        case 4:
		        	int [][] deteccionB = {{-0,1,0},{1,4,1},{0,1,0}};
		        	ExecutorService executor4 =  Executors.newCachedThreadPool();
			        for (int i=0;i<matriz.length;i++) {
		    		    executor4.execute(new RunnableTask(matriz,deteccionB,resultado,i,ancho-1));
		    		}
		    		executor4.shutdown();
		 	       	System.out.println("*** Imagen procesada correctamente *** ");
			        break;
		        case 5:
		        	int [][] emboss = {{-2,-1,0},{-1,1,1},{0,1,2}};
		        	ExecutorService executor5 =  Executors.newCachedThreadPool();
			        for (int i=0;i<matriz.length;i++) {
		    		    executor5.execute(new RunnableTask(matriz,emboss,resultado,i,ancho-1));
		    		}
		    		executor5.shutdown();
		 	       	System.out.println("*** Imagen procesada correctamente *** ");
			        break;
		        case 6:
		        	int [][] desenfocar = {{1,1,1},{1,1,1},{1,1,1}};
		        	ExecutorService executor6 =  Executors.newCachedThreadPool();
			        for (int i=0;i<matriz.length;i++) {
		    		    executor6.execute(new RunnableTask(matriz,desenfocar,resultado,i,ancho-1));
		    		}
		    		executor6.shutdown();
		 	       	System.out.println("*** Imagen procesada correctamente *** ");
			        break;
		        case 0:
		        	System.out.println("Saliendo...");
		        	break;
		        default:
					System.out.println("N�mero no reconocido");
					break;
				}
			        
		        
		      
		 
		 }
	       sc.close();
	       }
		
	

	

}
