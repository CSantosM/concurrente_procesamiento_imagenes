import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;


public class Main{

	public static void main(String[] args) throws IOException {
		ForkJoinPool pool = new ForkJoinPool();
		int ancho,alto,gris;
		String [][] matriz;
	       Scanner sc = new Scanner(System.in);
	       System.out.println("-------------------------------- Elige la ruta de la imagen --------------------------------");
	       String ruta = sc.nextLine();
	       Filter f = new Filter(ruta);
	       System.out.println("Procesando matriz, por favor espere...");
	       String dimension = f.dimension(ruta);
	       String [] pixel = dimension.split("\\s+");
	       ancho = Integer.parseInt(pixel[0]);
	       alto = Integer.parseInt(pixel[1]);
	       gris = Integer.parseInt(pixel[2]);
	       
	      
	       
	       matriz = new String [alto][ancho];
	       matriz = f.volcarMatriz(alto, ancho);
	       System.out.println("Matriz"+matriz.length+"x"+matriz[0].length);
	       int opcion = 2;
	       while(opcion != 0){
	    	   System.out.println("-------------------------------- Elige el filtro que desesas aplicar --------------------------------");
		       System.out.println("1 - Sobel Horizontal");
		       System.out.println("2 - Sobel Vertical");
		       System.out.println("3 - Enfocar");
		       System.out.println("4 - Deteccion de bordes");
		       System.out.println("5 - Emboss");
		       System.out.println("6 - Desenfocar");
		       opcion = Integer.parseInt(sc.nextLine());
		       System.out.println("*** Procesando, espere por favor... ***");
		       switch (opcion) {
		       
		        case 1:
		        	int [][] sobelH = {{-1,-2,-1},{0,0,0},{1,2,1}};
		        	String [][] result = (String[][]) pool.invoke(new FJFilter(matriz,sobelH,0,ancho-1));
		 	       	System.out.println("Matriz"+result.length+"x"+result[0].length);
		 	       	System.out.println("*** FIN DYV *** ");
		 	       	System.out.println("Escribe el nombre del fichero de salida (sin extension");
		 	       	String nombre = sc.nextLine();
		 	      	f.escribirFichero(result, alto, ancho,gris,nombre);
		 	       	System.out.println("*** Imagen procesada correctamente *** ");
		        break;
		        case 2:
			        int [][] sobelV = {{-1, 0, 1},{-2,0,2},{-1,0,1}};
			        String [][] result2 = (String[][]) pool.invoke(new FJFilter(matriz,sobelV,0,ancho-1));
		 	       	System.out.println("Matriz"+result2.length+"x"+result2[0].length);
		 	       	System.out.println("*** FIN DYV *** ");
		 	       System.out.println("Escribe el nombre del fichero de salida (sin extension");
		 	       	String nombre2 = sc.nextLine();
		 	       	f.escribirFichero(result2, alto, ancho,gris,nombre2);
		 	       	System.out.println("*** Imagen procesada correctamente *** ");
		        break;
		        case 3:
		        	int [][] enfocar = {{0, -1,0},{-1,5,-1},{0,-1,0}};
		        	String [][] result3 = (String[][]) pool.invoke(new FJFilter(matriz,enfocar,0,ancho-1));
		 	       	System.out.println("Matriz"+result3.length+"x"+result3[0].length);
		 	       	System.out.println("*** FIN DYV *** ");
		 	       System.out.println("Escribe el nombre del fichero de salida (sin extension");
		 	       	String nombre3 = sc.nextLine();
		 	      	f.escribirFichero(result3, alto, ancho,gris,nombre3);
		 	       	System.out.println("*** Imagen procesada correctamente *** ");
			        break;
		        case 4:
		        	int [][] deteccionB = {{-0,1,0},{1,4,1},{0,1,0}};
		        	String [][] result4 = (String[][]) pool.invoke(new FJFilter(matriz,deteccionB,0,ancho-1));
		 	       	System.out.println("Matriz"+result4.length+"x"+result4[0].length);
		 	       	System.out.println("*** FIN DYV *** ");
		 	       System.out.println("Escribe el nombre del fichero de salida (sin extension");
		 	       	String nombre4 = sc.nextLine();
		 	      	f.escribirFichero(result4, alto, ancho,gris,nombre4);
		 	       	System.out.println("*** Imagen procesada correctamente *** ");
			        break;
		        case 5:
		        	int [][] emboss = {{-2,-1,0},{-1,1,1},{0,1,2}};
		        	String [][] result5 = (String[][]) pool.invoke(new FJFilter(matriz,emboss,0,ancho-1));
		 	       	System.out.println("Matriz"+result5.length+"x"+result5[0].length);
		 	       	System.out.println("*** FIN DYV *** ");
		 	       System.out.println("Escribe el nombre del fichero de salida (sin extension");
		 	       	String nombre5 = sc.nextLine();
		 	      	f.escribirFichero(result5, alto, ancho,gris,nombre5);
		 	       	System.out.println("*** Imagen procesada correctamente *** ");
			        break;
		        case 6:
		        	int [][] desenfocar = {{1,1,1},{1,1,1},{1,1,1}};
		        	String [][] result6 = (String[][]) pool.invoke(new FJFilter(matriz,desenfocar,0,ancho-1));
		 	       	System.out.println("Matriz"+result6.length+"x"+result6[0].length);
		 	       	System.out.println("*** FIN DYV *** ");
		 	       System.out.println("Escribe el nombre del fichero de salida (sin extension");
		 	       	String nombre6 = sc.nextLine();
		 	      	f.escribirFichero(result6, alto, ancho,gris,nombre6);
		 	       	System.out.println("*** Imagen procesada correctamente *** ");
			        break;
		        case 0:
		        	System.out.println("Saliendo...");
		        	break;
		        default:
					System.out.println("N�mero no reconocido");
					break;
				}
			        
		        
		      
		 
		 }
	       sc.close();
	       }
		
	

	

}
